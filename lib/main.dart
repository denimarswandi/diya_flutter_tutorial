import 'package:flutter/material.dart';

void main() {
  runApp(new MaterialApp(
    title: "Card & Parsing",
    home: new HalamanSatu(),
  ));
}

class HalamanSatu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Card & Parsing"),
      ),
      body: new Container(
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            new CardSaya(icon: Icons.home, text: "Home", warna: Colors.brown,),
            new CardSaya(icon: Icons.favorite, text: "Favorite", warna: Colors.pink,),
            new CardSaya(icon: Icons.place, text: "Flace", warna: Colors.blue,),
            new CardSaya(icon: Icons.settings, text: "Settings", warna: Colors.black,),
          
          ],
        ),
      ),
    );
  }
}

class CardSaya extends StatelessWidget {
  CardSaya({this.icon, this.warna ,this.text});
  final IconData icon;
  final String text;
  final Color warna;


  @override
  Widget build(BuildContext context) {
    return new Container(
      padding: new EdgeInsets.all(10.0),
      child: new Card(
        child: new Column(
          children: <Widget>[
            new Icon(icon, size: 50.0, color: warna),
            new Text(text, style: new TextStyle(fontSize: 20.0))
          ],
        ),
      ),
    );
  }
}
